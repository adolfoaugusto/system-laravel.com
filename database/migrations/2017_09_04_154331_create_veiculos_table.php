<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVeiculosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('veiculos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('placa', 8);
            $table->string('renavam', 11);
            $table->string('chassi', 20);
            $table->text('municipioEmplacamento');
            $table->integer('anoFabricacao');
            $table->string('numeroMotor', 25);
            $table->integer('anoModelo');
            $table->text('marca');
            $table->text('cor');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('veiculos');
    }
}
