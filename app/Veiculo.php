<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Veiculo extends Model
{
    protected $fillable = [
   		'placa',
        'renavam',
        'chassi',
        'municipioEmplacamento',
        'anoFabricacao',
        'numeroMotor',
        'anoModelo',
        'marca',
        'cor'
    ];

}
