<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('usuarios', 'UsuariosController@index');


Route::group(['middleware'=>'web'], function() {
	Route::get('/', 'HomeController@index');
	
	Route::auth();

	Route::get('/home', 'HomeController@index');

	Route::get('clientes', 'ClientesController@index');
	Route::get('clientes/novo', 'ClientesController@novo');
	Route::get('clientes/{cliente}/editar', 'ClientesController@editar');
	Route::post('clientes/salvar', 'ClientesController@salvar');	
	Route::patch('clientes/{cliente}', 'ClientesController@atualizar');
	Route::delete('clientes/{cliente}', 'ClientesController@deletar');

	Route::get('veiculos', 'VeiculosController@index');
	Route::get('veiculos/novo', 'VeiculosController@novo');
	Route::get('veiculos/{veiculo}/editar', 'VeiculosController@editar');
	Route::post('veiculos/salvar', 'VeiculosController@salvar');	
	Route::patch('veiculos/{veiculo}', 'VeiculosController@atualizar');
	Route::delete('veiculos/{veiculo}', 'VeiculosController@deletar');
});
