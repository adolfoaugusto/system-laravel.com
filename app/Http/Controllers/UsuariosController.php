<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class UsuariosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('usuarios', [
            'texto'=>'Listagem de usuários',
            'checagem'=>true,
            'usuarios'=>['usuarios1', 'usuarios2', 'usuarios3', 'usuarios4']
        ]);
    }

    
}
