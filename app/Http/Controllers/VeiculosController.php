<?php

namespace App\Http\Controllers;

use App\Veiculo;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;

class VeiculosController extends Controller
{
    public function index()
    {
        $veiculos = Veiculo::get();

        return view('veiculos.lista', ['veiculos'=> $veiculos]);
    }
    
    public function novo()
    {
        return view('veiculos.formulario');
    }

    public function salvar(Request $request)
    {
        $veiculo = new Veiculo;

        $veiculo = $veiculo->create( $request->all() );

        \Session::flash('mensagem_sucesso', 'Veículo cadastrado com sucesso!');

        return Redirect::to('veiculos/novo');

        // var_dump($request);

    }
    public function editar($id)
    {
        $veiculo = Veiculo::findOrFail($id);

        return view('veiculos.formulario', ['veiculo'=>$veiculo]);
    }
    public function atualizar($id, Request $request)
    {
        $veiculo = Veiculo::findOrFail($id);
        
        $veiculo->update( $request->all() );

        \Session::flash('mensagem_sucesso', 'Veículo atualizado com sucesso!');

        return Redirect::to('veiculos/'.$veiculo->id.'/editar');
    }
    public function deletar($id)
    {
        $veiculo = Veiculo::findOrFail($id);
        
        $veiculo->delete();

        \Session::flash('mensagem_sucesso', 'Veículo Deletado com sucesso!');
        
        return Redirect::to('veiculos');
    }
}
