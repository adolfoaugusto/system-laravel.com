@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">
                Informe a baixo as informações do cliente
                    <a href="{{ url('clientes') }}" class="pull-right">Listagem Cliente</a>
                </div>

                <div class="panel-body">
                    @if(Session::has('mensagem_sucesso') )
                        <div class="alert alert-success">{{ Session::get('mensagem_sucesso') }}</div>
                    @endif

                    @if(Request::is('*/editar'))
                        {!! Form::model($cliente, ['method'=>'PATCH', 'url'=>'clientes/'.$cliente->id]) !!}
                    @else
                        {!! Form::open(['url'=>'clientes/salvar']) !!}
                    @endif
                        
                        {!! Form::label('name', 'Nome')  !!}
                        {!! Form::input('text', 'name', null, ['class'=>'form-control', 'autofocus', 'placeholder' =>'Fulano do Santos']) !!}

                        {!! Form::label('adress', 'Endereço')  !!}
                        {!! Form::input('text', 'adress', null, ['class'=>'form-control', 'autofocus', 'placeholder' =>'Rua Norte']) !!}

                        {!! Form::label('number', 'Número')  !!}
                        {!! Form::input('text', 'number', null, ['class'=>'form-control', 'autofocus', 'placeholder' =>'0000']) !!}
                        <br>
                        {!! Form::submit('Salvar', ['class'=>'btn btn-primary pull-right']) !!}

                        {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection