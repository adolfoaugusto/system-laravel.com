@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">
                Clientes
                <a href="{{url('clientes/novo')}}" class="pull-right">Novo Cliente</a>
                </div>

                <div class="panel-body">
                    @if(Session::has('mensagem_sucesso') )
                        <div class="alert alert-success">{{ Session::get('mensagem_sucesso') }}</div>
                    @endif
                    <table class="table">
                        <th>ID</th>
                        <th>Nome</th>
                        <th>Endereço</th>
                        <th>Telefone</th>
                        <th>Ações</th>
                        <tbody>
                            @foreach($clientes as $cliente)
                            <tr>
                                <td>{{$cliente->id}}</td>
                                <td>{{$cliente->name}}</td>
                                <td>{{$cliente->adress}}</td>
                                <td>{{$cliente->number}}</td>
                                <td>   
                                    <a href="clientes/{{ $cliente->id }}/editar" class="btn btn-default btn-sm">Editar</a>
                                    {!! Form::open(['method'=>'DELETE' ,'url'=>'clientes/'.$cliente->id, 'style'=>'display: inline;']) !!}
                                    <button type="submit" class="btn btn-default btn-sm">Excluir</button>
                                    {!! Form::close() !!}
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
