@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">
                Clientes
                <a href="{{url('veiculos/novo')}}" class="pull-right">Novo Veículo</a>
                </div>

                <div class="panel-body">
                    @if(Session::has('mensagem_sucesso') )
                        <div class="alert alert-success">{{ Session::get('mensagem_sucesso') }}</div>
                    @endif
                    <table class="table">
                        <th>Placa</th>
                        <th>Munic. Emplac.</th>
                        <th>Marca</th>
                        <th>Ano Modelo</th>
                        <th>Cor</th>
                        <tbody>
                            @foreach($veiculos as $veiculo)
                            <tr>
                                <td>{{$veiculo->placa}}</td>
                                <td>{{$veiculo->municipioEmplacamento}}</td>
                                <td>{{$veiculo->marca}}</td>
                                <td>{{$veiculo->anoModelo}}</td>
                                <td>{{$veiculo->cor}}</td>
                                <td>   
                                    <a href="veiculos/{{ $veiculo->id }}/editar" class="btn btn-default btn-sm">Editar</a>
                                    {!! Form::open(['method'=>'DELETE' ,'url'=>'veiculos/'.$veiculo->id, 'style'=>'display: inline;']) !!}
                                    <button type="submit" class="btn btn-default btn-sm">Excluir</button>
                                    {!! Form::close() !!}
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
