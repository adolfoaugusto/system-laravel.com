@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">
                Cadastro de Veículos
                    <a href="{{ url('veiculos') }}" class="pull-right">Listagem Veículos</a>
                </div>

                <div class="panel-body">
                    @if(Session::has('mensagem_sucesso') )
                        <div class="alert alert-success">{{ Session::get('mensagem_sucesso') }}</div>
                    @endif

                    @if(Request::is('*/editar'))
                        {!! Form::model($veiculo, ['method'=>'PATCH', 'url'=>'veiculos/'.$veiculo->id]) !!}
                    @else
                        {!! Form::open(['url'=>'veiculos/salvar']) !!}
                    @endif

                        {!! Form::label('placa', 'Placa')  !!}
                        {!! Form::input('text', 'placa', null, ['class'=>'form-control', 'autofocus', 'placeholder' =>'EX: HUV 1245 ou HUV1245', 'style'=>'text-transform:uppercase', 'maxlength' => 8]) !!}

                        {!! Form::label('renavam', 'Renavam')  !!}
                        {!! Form::input('text', 'renavam', null, ['class'=>'form-control', 'autofocus', 'placeholder' =>'EX: 894578885']) !!}
                        
                        {!! Form::label('chassi', 'Chassi')  !!}
                        {!! Form::input('text', 'chassi', null, ['class'=>'form-control', 'autofocus', 'placeholder'=>'EX: 9BFZF16P868439003', 'style'=>'text-transform:uppercase']) !!}

                        {!! Form::label('municipioEmplacamento', 'Munic. Emplac.')  !!}
                        {!! Form::input('text', 'municipioEmplacamento', null, ['class'=>'form-control', 'autofocus', 'placeholder'=>'EX: FORTALEZA', 'style'=>'text-transform:uppercase']) !!}

                        {!! Form::label('anoFabricacao', 'Ano Fabricação')  !!}
                        {!! Form::input('number', 'anoFabricacao', null, ['class'=>'form-control', 'autofocus', 'placeholder' =>'EX: 1980', 'maxlength' => 4]) !!}

                        {!! Form::label('numeroMotor', 'Número Motor')  !!}
                        {!! Form::input('text', 'numeroMotor', null, ['class'=>'form-control', 'autofocus', 'placeholder'=>'EX: QFkA68439303', 'style'=>'text-transform:uppercase']) !!}

                        {!! Form::label('anoModelo', 'Ano Modelo')  !!}
                        {!! Form::input('number', 'anoModelo', null, ['class'=>'form-control', 'autofocus', 'placeholder' =>'EX: 1981', 'style'=>'text-transform:uppercase', 'maxlength' => 4]) !!}

                        {!! Form::label('marca', 'Marca')  !!}
                        {!! Form::input('text', 'marca', null, ['class'=>'form-control', 'autofocus', 'placeholder' =>'EX: FORD/FIESTA 1.6 FLEX']) !!}

                        {!! Form::label('cor', 'Cor')  !!}
                        {!! Form::input('text', 'cor', null, ['class'=>'form-control', 'autofocus', 'placeholder' =>'EX: VERMELHO', 'style'=>'text-transform:uppercase']) !!}
                        <br>
                        {!! Form::submit('Salvar', ['class'=>'btn btn-primary pull-right']) !!}

                        {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection